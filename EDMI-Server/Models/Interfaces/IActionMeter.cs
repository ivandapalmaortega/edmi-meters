﻿using System.Web.Mvc;

namespace EDMI_Server.Models
{
    internal interface IActionMeter<T>
    {
        ActionResult Add(T meter);

        ActionResult Delete(string id);
    }
}
