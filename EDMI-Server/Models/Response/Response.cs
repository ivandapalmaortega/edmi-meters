﻿using EDMI_Server.Configuration;
using System.ComponentModel.DataAnnotations;

namespace EDMI_Server.Models.Response
{
    public class Response
    {
        public string Status { get; set; }

        public string Description { get; set; }

        public Response()
        {
            this.Status = EDMIConfiguration.RESPONSE_OK;
            this.Description = string.Empty;
        }

        public Response(string description)
        {
            this.Status = EDMIConfiguration.RESPONSE_ERROR;
            this.Description = description;
        }

        public Response(string status, string description)
        {
            this.Status = status;
            this.Description = description;
        }

    }
}