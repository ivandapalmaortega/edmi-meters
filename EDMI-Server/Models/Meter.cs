﻿using System.ComponentModel.DataAnnotations;

namespace EDMI_Server.Models
{
    public class Meter
    {        
        [Required]
        public string Id { get; set; }

        [Required]
        public string SerialNumber { get; set; }

        [Required]
        public string Type { get; set; }

        public Meter() { }

        public Meter(string Id, string SerialNumber, string Type)
        {
            this.Id = Id;
            this.SerialNumber = SerialNumber;
            this.Type = Type;
        }
        
    }
}