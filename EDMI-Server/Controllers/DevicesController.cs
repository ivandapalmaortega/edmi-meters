﻿using EDMI_Server.Configuration;
using EDMI_Server.Models;
using EDMI_Server.Models.Response;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace EDMI_Server.Controllers
{
    public class DevicesController : ApiController
    {
        private static Logger log = LogManager.GetLogger("Logger");
        private List<Meter> meter = null;
        private readonly string directoryPath = null;
        private readonly string fileFullPath = null;
        private readonly string fileName = null;

        /// <summary>
        /// Constructor. Initialize all necessary elements such as all devices list or File path
        /// </summary>
        public DevicesController()
        {
            log.Debug("\nInitializing Device Controller...");

            try
            {
                // Get configuration from web.config
                fileName = ConfigurationManager.AppSettings["jsonFile"].ToString();
                directoryPath = ConfigurationManager.AppSettings["FolderPath"].ToString();

                if (string.IsNullOrWhiteSpace(directoryPath))
                {
                    directoryPath = Directory.GetCurrentDirectory();
                }

                // Create directory if does not exist
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                // Creating file full path with format
                if (!string.IsNullOrWhiteSpace(fileName))
                {
                    this.fileFullPath = string.Format("{0}\\{1}", this.directoryPath, fileName);
                }

                if (File.Exists(fileFullPath))
                {
                    string fileContent = File.ReadAllText(fileFullPath);
                    if (!string.IsNullOrWhiteSpace(fileContent))
                    {
                        meter = JsonConvert.DeserializeObject<List<Meter>>(fileContent);
                    }
                }
                else
                {
                    meter = new List<Meter>();
                }
            }
            catch(Exception ex)
            {
                log.Debug(string.Format("Error in Initialization. Message=[{0}]", ex.Message));
            }
            
        }

        /// <summary>
        /// Get all devices
        /// </summary>
        /// <returns></returns>
        [Route("api/devices")]
        public IEnumerable<Meter> GetDevices()
        {
            log.Debug("Call Method GetDevices");
            return meter;
        }

        /// <summary>
        /// Get Devices accoring to a type
        /// </summary>
        /// <param name="type">Devices Type</param>
        /// <returns>List of devices according to a type</returns>
        [Route("api/devices/{type}")]
        public IEnumerable<Meter> GetDevices(string type)
        {
            log.Debug(string.Format("Call Method GetDevices with Type: {0}", type));
            IEnumerable<Meter> meters = (List<Meter>)(from x in meter where (x.Type.Equals(type.ToLower())) select x).ToList();
            return meters;
        }

        /// <summary>
        /// Add new devices (they can be more than one)
        /// </summary>
        /// <param name="value">New devices in Json format</param>
        [Route("api/devices/new")]
        public HttpResponseMessage Post([FromBody] JToken postData, HttpRequestMessage message)
        {
            log.Debug("Method Devices/New");
            Response response = null;
            HttpResponseMessage httpResponse = null;
            string jsonResponse = null;

            try
            {   
                // New meters to be added
                Meter newMeter = JsonConvert.DeserializeObject<Meter>(postData.ToString());
                log.Debug(string.Format("REQUEST:\n{0}", postData.ToString()));

                // Adding meters

               
                List<Meter> tempMeterList = (List<Meter>)(from x in this.meter where x.Id.Equals(newMeter.Id) select x).ToList();
                if (tempMeterList.Count == 0)
                {
                    this.meter.Add(newMeter);
                }
                else
                {
                    response = new Response(EDMIConfiguration.RESPONSE_OK, EDMIConfiguration.DESCRIPTION_EXISTING_DEVICE);
                    jsonResponse = JsonConvert.SerializeObject(response);
                    httpResponse = Request.CreateResponse(HttpStatusCode.OK);
                    httpResponse.Content = new StringContent(jsonResponse, Encoding.UTF8, "application/json");
                    log.Debug(EDMIConfiguration.DESCRIPTION_EXISTING_DEVICE);
                    return httpResponse;
                }
                
            }
            catch (Exception ex)
            {
                log.Debug(string.Format("Error trying to deserialize request or adding new meters. Message=[{0}]", ex.Message));
            }

            try
            {
                // Response Settings
                response = new Response();
                jsonResponse = JsonConvert.SerializeObject(response);
                httpResponse = Request.CreateResponse(HttpStatusCode.OK);

                string jsonMeter = JsonConvert.SerializeObject(meter, Formatting.Indented);
                
                // Save to file
                File.WriteAllText(fileFullPath, jsonMeter);

                log.Debug(string.Format("RESPONSE:\n{0}", jsonMeter));
            }
            catch(Exception ex)
            {
                response = new Response(ex.Message);
                jsonResponse = JsonConvert.SerializeObject(response);
                httpResponse = Request.CreateResponse(HttpStatusCode.BadRequest);
                log.Debug(string.Format("Error trying to Serialize response or writing in file. Message=[{0}]", ex.Message));
            }

            httpResponse.Content = new StringContent(jsonResponse, Encoding.UTF8, "application/json");
            return httpResponse;
        }


    }
}
