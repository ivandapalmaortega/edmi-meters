﻿namespace EDMI_Server.Configuration
{
    public static class EDMIConfiguration
    {
        // Error
        public static readonly string RESPONSE_OK = "Success";
        public static readonly string RESPONSE_ERROR = "Error";

        // Description
        public static readonly string DESCRIPTION_EXISTING_DEVICE = "Device already exist";
    }
}