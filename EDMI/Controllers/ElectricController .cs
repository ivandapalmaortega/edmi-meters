﻿using EDMI.EDMIConfiguration;
using EDMI.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EDMI.Controllers
{
    public class ElectricController : Controller, IActionMeter<Meter>
    {
        private static List<Meter> meters = null;
        private static Logger log = LogManager.GetLogger("Logger");

        public ActionResult Index()
        {
            if(meters == null)
                meters = GetElectricMeters();

            return View(meters);
        }

        [NonAction]
        private List<Meter> GetElectricMeters()
        {
            log.Debug("Getting Electric Meters...");
            List<Meter> electrics = null;
            string jsonMeters = null;

            try
            {
                jsonMeters = Utils.GetDevices(Constant.TYPE_ELECTRIC);
                electrics = JsonConvert.DeserializeObject<List<Meter>>(jsonMeters);
                log.Debug(string.Format("Get Electric Meters call. Meters=[{0}]", jsonMeters));
            }
            catch(Exception ex)
            {
                electrics = new List<Meter>();
                log.Debug("Error trying to get Electric meters. Message=[{0}]", ex.Message);
            }

            return electrics;
        }

        public ActionResult Add()
        {
            ViewBag.Message = "Add new Electric Meter";
            return View();
        }

        [HttpPost]
        public ActionResult Add(Meter meter)
        {
            log.Debug("Adding new Electric Meter");
            if (meters != null && meter != null)
            {
                try
                {
                    string url = Constant.URL;
                    meter.Type = Constant.TYPE_ELECTRIC;
                    string jsonMeters = Utils.AddDevices(meter);
                    Response response = JsonConvert.DeserializeObject<Response>(jsonMeters);

                    if (response.Status.Equals(Constant.RESPONSE_SUCCESS))
                    {
                        if (string.IsNullOrWhiteSpace(response.Description))
                        {
                            log.Debug("Device added properly");
                            meters.Add(meter);
                        }
                        else
                        {
                            log.Debug(response.Description);
                        }
                    }
                    else
                    {
                        log.Debug(string.Format("Error trying to Add new Electric Meters. Message=[{0}]", response.Description));
                    }
                }
                catch(Exception ex)
                {
                    log.Debug(string.Format("Error trying to Add new Electric Meters. Message=[{0}]", ex.Message));
                }

            }
            return RedirectToAction("Index");
        }

    }
}