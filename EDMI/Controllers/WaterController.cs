﻿using EDMI.EDMIConfiguration;
using EDMI.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EDMI.Controllers
{
    public class WaterController : Controller, IActionMeter<Meter>
    {
        private static List<Meter> meters = null;
        private static Logger log = LogManager.GetLogger("Logger");

        public ActionResult Index()
        {
            if (meters == null)
                meters = GetWaterMeters();

            return View(meters);
        }

        [NonAction]
        private List<Meter> GetWaterMeters()
        {
            log.Debug("Getting Water Meters...");
            List<Meter> Waters = null;
            string jsonMeters = null;

            try
            {
                jsonMeters = Utils.GetDevices(Constant.TYPE_WATER);
                Waters = JsonConvert.DeserializeObject<List<Meter>>(jsonMeters);
                log.Debug(string.Format("Get Water Meters call. Meters=[{0}]", jsonMeters));
            }
            catch (Exception ex)
            {
                Waters = new List<Meter>();
                log.Debug("Error trying to get Water meters. Message=[{0}]", ex.Message);
            }

            return Waters;
        }

        public ActionResult Add()
        {
            ViewBag.Message = "Add new Water Meter";
            return View();
        }

        [HttpPost]
        public ActionResult Add(Meter meter)
        {
            log.Debug("Adding new Water Meter");
            if (meters != null && meter != null)
            {
                try
                {
                    string url = Constant.URL;
                    meter.Type = Constant.TYPE_WATER;
                    string jsonMeters = Utils.AddDevices(meter);
                    Response response = JsonConvert.DeserializeObject<Response>(jsonMeters);

                    if (response.Status.Equals(Constant.RESPONSE_SUCCESS))
                    {
                        if (string.IsNullOrWhiteSpace(response.Description))
                        {
                            log.Debug("Device added properly");
                            meters.Add(meter);
                        }
                        else
                        {
                            log.Debug(response.Description);
                        }
                    }
                    else
                    {
                        log.Debug(string.Format("Error trying to Add new Water Meters. Message=[{0}]", response.Description));
                    }
                }
                catch (Exception ex)
                {
                    log.Debug(string.Format("Error trying to Add new Water Meters. Message=[{0}]", ex.Message));
                }

            }
            return RedirectToAction("Index");
        }

    }
}