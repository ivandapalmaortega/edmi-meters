﻿using EDMI.EDMIConfiguration;
using EDMI.Models;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EDMI.Controllers
{
    public class GatewayController : Controller, IActionMeter<Meter>
    {
        private static List<Meter> meters = null;
        private static Logger log = LogManager.GetLogger("Logger");

        public ActionResult Index()
        {
            if(meters == null)
                meters = GetGatewayMeters();

            return View(meters);
        }

        [NonAction]
        private List<Meter> GetGatewayMeters()
        {
            log.Debug("Getting Gateway Meters...");
            List<Meter> gatewayss = null;
            string jsonMeters = null;

            try
            {
                jsonMeters = Utils.GetDevices(Constant.TYPE_GATEWAY);
                gatewayss = JsonConvert.DeserializeObject<List<Meter>>(jsonMeters);
                log.Debug(string.Format("Get Gateway Meters call. Meters=[{0}]", jsonMeters));
            }
            catch(Exception ex)
            {
                gatewayss = new List<Meter>();
                log.Debug("Error trying to get Gateway meters. Message=[{0}]", ex.Message);
            }

            return gatewayss;
        }

        public ActionResult Add()
        {
            ViewBag.Message = "Add new Gateway Meter";
            return View();
        }

        [HttpPost]
        public ActionResult Add(Meter meter)
        {
            log.Debug("Adding new Gateway Meter");
            if (meters != null && meter != null)
            {
                try
                {
                    string url = Constant.URL;
                    meter.Type = Constant.TYPE_GATEWAY;
                    string jsonMeters = Utils.AddDevices(meter);
                    Response response = JsonConvert.DeserializeObject<Response>(jsonMeters);

                    if (response.Status.Equals(Constant.RESPONSE_SUCCESS))
                    {
                        if (string.IsNullOrWhiteSpace(response.Description))
                        {
                            log.Debug("Device added properly");
                            meters.Add(meter);
                        }
                        else
                        {
                            log.Debug(response.Description);
                        }
                    }
                    else
                    {
                        log.Debug(string.Format("Error trying to Add new Gateway Meters. Message=[{0}]", response.Description));
                    }
                }
                catch(Exception ex)
                {
                    log.Debug(string.Format("Error trying to Add new Gateway Meters. Message=[{0}]", ex.Message));
                }

            }
            return RedirectToAction("Index");
        }

    }
}