﻿using System.Collections.Generic;

namespace EDMI.Models
{
    interface IMeter
    {
        string Id { get; set; }

        string SerialNumber { get; set; }

        string Type { get; set; }
    }
}
