﻿using System.Web.Mvc;

namespace EDMI.Models
{
    internal interface IActionMeter<T>
    {
        ActionResult Add(T meter);

    }
}
