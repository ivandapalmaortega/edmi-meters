﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EDMI.Models
{
    public class Response
    {
        public string Status { get; set; }

        public string Description { get; set; }

        public Response()
        {
            this.Status = "Success";
            this.Description = "Success";
        }

        public Response(string description)
        {
            this.Status = "Error";
            this.Description = description;
        }

    }
}