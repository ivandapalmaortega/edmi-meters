﻿using System.ComponentModel.DataAnnotations;

namespace EDMI.Models
{
    public class Meter : IMeter
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string SerialNumber { get; set; }

        public string Type { get; set; }

        public Meter() { }

        public Meter (string Id, string SerialNumber, string Type)
        {
            this.Id = Id;
            this.SerialNumber = SerialNumber;
            this.Type = Type;
        }
    }
}