﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace EDMI.EDMIConfiguration
{
    public static class Constant
    {
        // Types
        public static readonly string TYPE_ELECTRIC = "electric";
        public static readonly string TYPE_WATER = "water";
        public static readonly string TYPE_GATEWAY = "gateway";
        public static readonly string TYPE_NEW = "new";

        // Methods
        public static readonly string METHOD_GET = "GET";
        public static readonly string METHOD_POST = "POST";

        // Routing


        // Responses Status
        public static readonly string RESPONSE_SUCCESS = "Success";
        public static readonly string RESPONSE_ERROR = "Error";

        // AppSettings
        public static readonly string URL = ConfigurationManager.AppSettings["url"].ToString();

    }
}