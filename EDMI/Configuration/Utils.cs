﻿using Newtonsoft.Json;
using NLog;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace EDMI.EDMIConfiguration
{
    public static class Utils
    {
        private static Logger log = LogManager.GetLogger("Logger");

        public static string GetDevices(string type)
        {
            string url = string.Format("{0}/{1}", Constant.URL, type);
            string response = SendRequest(url, string.Empty, Constant.METHOD_GET);
            return response;
            
        }
        public static string AddDevices(object body)
        {
            string fullURL = string.Format("{0}/new", Constant.URL);
            string response = SendRequest(fullURL, body, Constant.METHOD_POST);
            return response;
        }



        public static string SendRequest(string url, object body, string method)
        {
            log.Debug("Sending request to EDMI-Server. URL: ({0})", url);

            UTF8Encoding encoding = new UTF8Encoding();
            HttpWebRequest webRequest = null;
            WebResponse webResponse = null;
            string stringResponse = string.Empty;

            // Send request
            try
            {
                /* In case of TLS 1.2
              ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });
              ServicePointManager.SecurityProtocol = Tls12;
              */

                // Preparing WebRequest and request
                webRequest = WebRequest.Create(url) as HttpWebRequest;
                webRequest.Method = method.ToUpper();
                webRequest.ContentType = "application/json";
                webRequest.Accept = "application/json";

                if (method.Equals(Constant.METHOD_POST))
                {
                    string requestBody = JsonConvert.SerializeObject(body,
                                         Formatting.None,
                                         new JsonSerializerSettings
                                         {
                                             TypeNameHandling = TypeNameHandling.Auto,
                                             NullValueHandling = NullValueHandling.Ignore
                                         });

                    log.Debug("Request Body : \n\t{0}\n\t", requestBody);

                    if (!(string.IsNullOrEmpty(requestBody)))
                    {
                        using (var streamWriter = new StreamWriter(webRequest.GetRequestStream()))
                        {
                            streamWriter.Write(requestBody);
                        }
                    }
                }                

                log.Debug("WebClient: Contacting EDMI-Server...");

                webResponse = webRequest.GetResponse();

                using (Stream stream = webResponse.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(stream))
                    {
                        stringResponse = sr.ReadToEnd();
                        sr.Close();
                    }
                }

                log.Debug("Response body : \n\t{0}\n\t", stringResponse.ToString());

            }
            catch(WebException ex)
            {
                log.Debug("Error trying to call EDMI-Meter server. Message=[{0}]", ex.Message);

                switch (ex.Status)
                {
                    case WebExceptionStatus.Timeout:                        
                        log.Error("Sending request to EDMI-Server: Timeout. [{0}]", ex.Message);                      
                        break;
                    case WebExceptionStatus.ProtocolError:
                        log.Error("Response from EDMI-Server:\n\t{0}\n\t", (new StreamReader(ex.Response.GetResponseStream(), encoding)).ReadToEnd());
                        break;
                    default:
                        log.Error("Error sending request to TBP: ", ex);
                        break;
                }

            }

            return stringResponse;
        }


    }
}