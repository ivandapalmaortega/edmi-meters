﻿using EDMI_Server.Controllers;
using EDMI_Server.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Web.Mvc;
using UnitTestingEDMI.Configuration;

namespace UnitTestingEDMI.Controllers
{
    [TestClass]
    public class DevicesControllerTest
    {

        [TestMethod]
        public void GetDevices()
        {

            // Arrange
            DevicesController controller = new DevicesController();

            // Act
            IEnumerable<Meter> meters = controller.GetDevices();

            // Assert
            Assert.IsNotNull(meters);
        }

        [TestMethod]
        public void GetElectricDevices()
        {
            // Arrange
            DevicesController controller = new DevicesController();

            // Act
            IEnumerable<Meter> meters = controller.GetDevices(Constant.TYPE_ELECTRIC);

            // Assert
            Assert.IsNotNull(meters);
        }

        [TestMethod]
        public void GetWaterDevices()
        {
            // Arrange
            DevicesController controller = new DevicesController();

            // Act
            IEnumerable<Meter> meters = controller.GetDevices(Constant.TYPE_WATER);

            // Assert
            Assert.IsNotNull(meters);
        }

        [TestMethod]
        public void GetGatewayDevices()
        {
            // Arrange
            DevicesController controller = new DevicesController();

            // Act
            IEnumerable<Meter> meters = controller.GetDevices(Constant.TYPE_GATEWAY);

            // Assert
            Assert.IsNotNull(meters);
        }

    }
}