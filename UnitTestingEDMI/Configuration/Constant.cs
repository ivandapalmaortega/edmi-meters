﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingEDMI.Configuration
{
    public static class Constant
    {
        public static readonly string TYPE_ELECTRIC = "electric";
        public static readonly string TYPE_WATER = "water";
        public static readonly string TYPE_GATEWAY = "gateway";
    }
}
