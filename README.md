# EDMI-Meters

This README is a guide that shows the necessary steps to get the application up and running.

<br/>

## What is this repository for?

* This repository contains a solution for collecting information about different kind of meters. They could be electric meters, water meters or gateway meters.

* Version: v1.0.0.1

<br/>

## How do I get set up?

The solution consists of two projects:
 
- One client project made in ASP.NET MVC (using Bootstrap) which performs REST calls.
- One Web API server project, which will response to all request made.


Possibilites:

    - Open two instance of Visual Studio (one client and one server).
    - Create a new Site on IIS for the Server and open client in Visual Studio.

<br/>

##  EDMI Client configuration

    1. Go to Web.config file
    2. Edit the url key within appSettings tag with the corresponding server location where client will try to connect. 
    Edit only the host:ip part. Do not touch /api/devices part.

<br/>

##  EDMI Server configuration
 
    1. Go to Web.config file
    2. Edit FolderPath key within appSettings tag with the corresponding path where you want the json file to be created. 
    It will contains all the devices added through the application.

<br/>

##  EDMI Unit Test configuration
 
Unit test project points to server project and it has its own config file. So we must add the keys in server web.config to our app.config file.
 
    1. Go to Web.config file
    2. Edit path folder key within appSettings tag with the corresponding path where you want the json file to be created. 
    It will contains all the devices added through the application. 
    Add both FolderPath and jsonFile keys as they are in server web.config file.
    3. Right click in Unit Test
    4. Execute Test (Ejecutar pruebas in Spanish)

<br/>

### Owner

* Iván Da Palma Ortega